import React, { Component } from 'react';
import { View, Text, ActivityIndicator, FlatList, ScrollView } from 'react-native';
import { styles } from '../styesheet/style';
import { getMovies } from '../connectivity/api';
import { getMoviesAction } from '../actions/getMoviesAction'
import { searchStatusAction } from '../actions/getSearchStatus'
import { connect } from 'react-redux'
// import {setImageData} from '../actions/setImageData'
import Flatlist from './Flatlist';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { heightPercentageToDP as hp } from 'react-native-responsive-screen';
import AsyncStorage from '@react-native-community/async-storage'; 

class MoviesList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      list: [],
      loader: true,
      totalResults: '',
      showMore: false,
     
    };
  }


   componentDidMount() {
    this.getMoviesFunction()
  // this._unsubscribe = this.props.navigation.addListener('focus', async () => {
    
  //   // this.compare()
  
  //   })
  }


  getMoviesFunction = () => {
    getMovies(undefined)
      .then(res => {
       
        this.setState({
          loader: false,
          totalResults: res.data.totalResults,
          list : res.data.Search
        })

        this.props.getMoviesAction(res.data.Search)
        // this.props.getMoviesAction(arr)
      })
      .catch(err => {
        this.setState({
          loader: false
        })
        console.warn(err)
      })
  }
  

  
  componentWillUnmount() {
    // this._unsubscribe()
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.list !== this.props.list) {
      console.warn('here')
      this.setState({
        loader: false,
        list : this.props.list
      },()=>console.warn("snew stae", this.state.list))
    }
    if (prevProps.status !== this.props.status) {
      console.warn('here')
      this.props.searchStatusAction(false)
      this.getMoviesFunction()
    }

   
  }


  render() {
    console.warn(this.props.list, 'LIST')
    return (
      <View style={this.props.style}>
        <View style={styles.movieContainer}>
          {this.state.loader ?
            <ActivityIndicator color="blue" />
            :
           
              <FlatList style={{ paddingBottom: hp('1%') }}
                numColumns={2}
                data={this.state.list}
                renderItem={({ item }) => (
                  <Flatlist {...item} {...this.props} />
                )}
              />

            

           }
        </View>
      </View>
    );
  }
}


const mapStateToProps = (state) => {
  return {
    list: state.getMoviesReducer.movies,
    status: state.getSearchStatusReducer.status,
    imageArr : state.getMoviesReducer.imageArr
  }
}

export default connect(mapStateToProps, { getMoviesAction, searchStatusAction })(MoviesList)