import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';
import { styles } from '../styesheet/style';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { shortListAction } from '../actions/shortListAction';
import { connect } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import { getMoviesAction } from '../actions/getMoviesAction'
import { cos } from 'react-native-reanimated';

class Flatlist extends Component {
    constructor(props) {
        super(props);
        this.state = {
            text: 'Short Listed',
            pressed: false
        };
    }

    shortList =async (id) => {
        console.warn('shortlist called');
        let oneData = this.props.list.filter(item => item.imdbID === id);
       
        console.warn("oneData",oneData)
        
        console.warn(typeof oneData )
        let asyncData = await AsyncStorage.getItem('Shortlist');

        if (asyncData === null || asyncData===undefined ) {
          await AsyncStorage.setItem('Shortlist', JSON.stringify(oneData));
        } else {
            asyncData= JSON.parse(asyncData);
          console.warn("oldData", asyncData);
          let newData = [...asyncData, ...oneData];
          console.warn("newData",newData)
          await AsyncStorage.setItem('Shortlist', JSON.stringify(newData));
        }

        this.compare();
    }


    compare=async()=>{
        console.warn("m i eerrr")
        let arr = []
        let asyncData = await AsyncStorage.getItem('Shortlist');
         asyncData = JSON.parse(asyncData)
         console.warn("asyncData",asyncData)
         for(let item of this.props.list){
             let isLiked = false;
            for(let asyncItem of asyncData){
                if(asyncItem.imdbID === item.imdbID){
                    isLiked = true;
                }
            }

            if(isLiked){
                arr.push({...item,shorlisted:true})
            }else{
                arr.push({...item,shorlisted:false})
            }
         }
    console.warn("updated arr",arr)
         this.props.getMoviesAction(arr)
    }
   

    componentDidMount() {
        this._unsubscribe = this.props.navigation.addListener('focus', async () => {

        })
    }

    componentWillUnmount() {
        this._unsubscribe()
    }

    render() {
        // console.warn('item', this.props)
        return (
            <View style={styles.flatListContainer}>
                {this.props?.Poster === "N/A" ?
                    <View style={{ width: wp('50%'), height: hp('15%'), justifyContent: 'center' }}>
                        <Text style={{ textAlign: 'center' }}>
                            N/A
                    </Text>
                    </View>
                    :
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('ImageGridScreen', { id: this.props.imdbID })}>
                        <Image style={{ width: wp('40%'), height: hp('15%') }} source={{ uri: this.props.Poster }}
                            resizeMode="contain" />
                    </TouchableOpacity>
                }

                <Text style={{ color: 'grey' }} >
                    {this.props.Title}
                </Text>
                <Text style={{ color: 'grey' }}>
                    Year : {this.props.Year}
                </Text>
                <Text style={{ color: 'grey' }}>
                    imdbID : {this.props.imdbID}
                </Text>
                <Text style={{ color: 'grey' }}>
                    Type : {this.props.Type}
                </Text>
                {/* {this.props.shortListing && <Text></Text>} */}


              {this.props.shortListing ? 
              null :
              
                this.props.shorlisted !== null && this.props.shorlisted === true  ? (
                    <Text>
                        Shortlisted
                    </Text>
                )
                    :
                    (<TouchableOpacity onPress={() => this.shortList(this.props.imdbID)}>
                        <Text style={{ color: 'blue', fontWeight: 'bold' }}>
                            Shortlist
                        </Text>
                    </TouchableOpacity>)
            }
                <View>
                </View>
            </View>
        );
    }
}

const mapStateToProps = (state) => {
    return { list: state.getMoviesReducer.movies }
}
export default connect(mapStateToProps, { shortListAction, getMoviesAction})(Flatlist)