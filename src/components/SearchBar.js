import React, { Component } from 'react';
import { View, Text,TextInput,TouchableOpacity } from 'react-native';
import { styles } from '../styesheet/style';
import {connect} from 'react-redux'
import { searchAction } from '../actions/searchAction';
import {searchStatusAction} from '../actions/getSearchStatus'
import { getMovies } from '../connectivity/api';
import {getMoviesAction} from '../actions/getMoviesAction'
 class SearchBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
        value  : '',
        searchState  : 'Search',
        error:""
    };
  }

  search=()=>{
  if(this.state.value !==""){
    getMovies(this.state.value)
    .then(res=>{
      console.warn("mnj",this.props.imageArr)
     
      
      
        this.props.getMoviesAction(res.data.Search)
    })
    .catch(err=>{
      this.setState({
        loader : false
      })
        console.warn(err)
    })
  }
   else {
     this.setState({error:'Enter something in search'})
   }
  }
clear=()=>{
  this.setState({
    value :'',
    error:''
  })
  this.props.searchStatusAction(true)
}

  render() {
    return (
     <View style={this.props.style}>
          <View style={styles.header}>
       <TextInput 
       style={styles.searchInputContainer} 
       value={this.state.value} 
       onChangeText={(text)=>this.setState({value : text}) }
       placeholder=" Search Here For Movies... "/>
       <View  style={styles.searchSubmitContainer}>
           <TouchableOpacity onPress={this.search}>
              <Text>
              {this.state.searchState}
              </Text>
           </TouchableOpacity>
       </View>
      
      </View>
    {this.state.error ? <Text style={{color:'red',textAlign:'center'}}>{this.state.error }</Text> :null}
      <TouchableOpacity onPress={this.clear}>
              <Text style={{textAlign:'center'}}>
              Clear Search
              </Text>
           </TouchableOpacity>
     </View>
    );
  }
}

const mapStateToProps = (state) =>{
    return {list : state.getMoviesReducer.movies,imageArr : state.getMoviesReducer.imageArr}
  }
  
  export default connect(mapStateToProps,{searchAction,searchStatusAction,getMoviesAction})(SearchBar)