import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

export const styles = StyleSheet.create({
    header : {
        width : wp('80%'),
        height :hp('5%'),
        borderWidth:1,
        borderRadius:50,
        // alignItems:'center'
        flexDirection:'row'

    },
    container : {
        flex:1,
        flexDirection:'column'
    },
    searchInputContainer : {
        width: '80%',
        paddingLeft:'2%',
        height :'100%',
    },
    flatListContainer : {
        width :wp('48%'),
        // height : hp('30%'),
        marginVertical : hp('1%'),
        alignItems:'center',
        justifyContent:'center',
        borderRadius : 20,
        marginHorizontal : wp('1%'),
        elevation :  7,
        borderColor : 'grey',
        backgroundColor : 'white'
    },
    searchSubmitContainer:{
        width: '20%',
        height :'100%',
        justifyContent:'center',
        alignItems:'center'
    }
})