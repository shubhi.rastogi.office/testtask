import axios from 'axios';
import BASE_URL from './baseURL'

export  function getMovies(payload) {
    return axios
        .get(BASE_URL+`${payload}`)
}