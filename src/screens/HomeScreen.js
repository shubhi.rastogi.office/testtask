import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import SearchBar from '../components/SearchBar';
import { styles } from '../styesheet/style';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import MoviesList from '../components/MoviesList';
import {setImageData} from '../actions/setImageData'
import { connect } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage'; 
 class HomeScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data : false
        };
    }

    // componentDidMount() {
    //     this._unsubscribe = this.props.navigation.addListener('focus', async () => {
    //     let list = await AsyncStorage.getItem('ShortList')
    //     if(list != null ) {
    //         list = JSON.parse(list)
    //         console.warn("asyns",list)
    //         this.props.setImageData(list)
    //     }
       
    //     })
    //   }
   
    //  componentWillUnmount(){
    //     this._unsubscribe()
    //  }
    render() {
        return (
            <View style={styles.container}>
                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                    <SearchBar />
                </View>
                <MoviesList  style={{ flex: 7 }} {...this.props} />
               
            </View>
        );
    }
}

export default connect(null,{setImageData})(HomeScreen)
