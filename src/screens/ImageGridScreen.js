import React, { Component } from 'react';
import { View, Text, Image,ActivityIndicator ,TouchableOpacity} from 'react-native';
import { searchAction } from '../actions/searchAction';
import { connect } from 'react-redux';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
// import {  } from 'react-native-gesture-handler';

 class ImageGridScreen extends Component {
  state ={
    loader : true
  }


  componentDidMount(){
    this.props.searchAction(this.props.route.params.id)
    // console.warn(this.props.route.params.id)
  }

  componentDidUpdate(prevProps,prevState) {
      if(prevProps.list !== this.props.list){
          this.setState({
            loader : false
          })
      }
  }
  render() {
    // console.warn(this.state.loader)
    return (
      <View style={{flex :1,backgroundColor:'black'}}>
        <View>
         <TouchableOpacity onPress={()=>this.props.navigation.goBack()}>
         <Text style={{color : 'white',padding:'2%'}}>
            Go Back
          </Text>
         </TouchableOpacity>
        </View>
        {!this.state.loader ?  <View style={{width: wp('100%'), height: hp('60%'),justifyContent:'center',alignItems:'center'}}>
          <Image style={{ width: wp('100%'), height: hp('50%') }} source={{ uri: this.props.list[0].Poster }}
                    resizeMode="contain" />
          </View> : <ActivityIndicator />}
     
      </View>
    );
  }
}


const mapStateToProps = (state) =>{
  return {list : state.getMoviesReducer.movies}
}

export default connect(mapStateToProps,{searchAction})(ImageGridScreen)