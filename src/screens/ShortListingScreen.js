import React, { Component } from 'react';
import { View, Text, FlatList, ActivityIndicator } from 'react-native';
import { connect } from 'react-redux';
import { shortListAction } from '../actions/shortListAction';
import { styles } from '../styesheet/style';
import Flatlist from '../components/Flatlist'
import AsyncStorage from '@react-native-community/async-storage'; 
import {setImageData} from '../actions/setImageData'

class ShortListingScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loader: true,
      list : []
    };
  }
 
  // componentDidMount(){
  //   console.warn("flatlist",this.props.list)
  // }
  componentDidMount() {
    this._unsubscribe = this.props.navigation.addListener('focus', async () => {
    let list = await AsyncStorage.getItem('Shortlist')
    console.warn(list,"list")
    if(list != null ) {
        list = JSON.parse(list)
      this.setState({list,loader:false})
    }
   
    })
  }

 componentWillUnmount(){
    this._unsubscribe()
 }

  
  render() {
    console.warn(this.state.list,"LISTBGG")
    return (
      <View>
        <View style={this.props.style}>
          <View style={styles.movieContainer}>
            {this.state.list.length == 0 ?
              <Text style={{textAlign:'center'}}>
                No data
              </Text> :
              this.state.loader ?
                <ActivityIndicator color="blue" />
                :
                <FlatList
                  numColumns={2}
                  data={this.state.list}
                  renderItem={( {item}  ) => (
                    console.warn(item,"item"),
                    // item.isShortlisted && <Flatlist shortListing={true}  {...item} {...this.props} />
                    <Flatlist shortListing={true}  {...item} {...this.props} />
                  )}
                />
                  }
            
          </View>
        </View>
      </View>
    );
  }
}
const mapStateToProps = (state) => {
  return { list: state.getMoviesReducer.movies }
}

export default connect(mapStateToProps, { shortListAction,setImageData })(ShortListingScreen)

