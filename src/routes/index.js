import * as React from 'react';
import { Text, View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import HomeScreenStack from './HomeScreenStack';
import ShortListingScreen from '../screens/ShortListingScreen';



const Tab = createBottomTabNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Tab.Navigator>
        <Tab.Screen name="HomeScreenStack" component={HomeScreenStack} />
        <Tab.Screen name="ShortListingScreen" component={ShortListingScreen} />
      </Tab.Navigator>
    </NavigationContainer>
  );
}