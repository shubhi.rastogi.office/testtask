import React from 'react'
import { View, Text } from 'react-native'
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'

import { SafeAreaProvider } from 'react-native-safe-area-context';
import HomeScreen from '../screens/HomeScreen'
import ImageGridScreen from '../screens/ImageGridScreen';

function HomeScreenStack(props) {
    const Stack = createStackNavigator();

    return (
        <SafeAreaProvider>
        
            <Stack.Navigator
             screenOptions={{
                headerShown: false}}
                >
                <Stack.Screen name="HomeScreen" component={HomeScreen} {...props} />
                <Stack.Screen name="ImageGridScreen" component={ImageGridScreen} {...props}/>
            </Stack.Navigator>
      
        </SafeAreaProvider>
       
    )

}

export default HomeScreenStack