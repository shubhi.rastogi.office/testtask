import {combineReducers} from "redux"
import getMoviesReducer from "./getMoviesReducer";
import getSearchStatusReducer from './getSearchStatusReducer'

const appReducer = combineReducers({
    getMoviesReducer,
    getSearchStatusReducer
})
const rootReducer = (state,action) =>{
    return appReducer(state,action);
}

export default rootReducer;