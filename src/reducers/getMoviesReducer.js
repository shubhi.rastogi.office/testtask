import AsyncStorage from '@react-native-community/async-storage';
const initialState = {
    movies: [],
    // shortlisted
    imageArr: []
}

const getMoviesReducer = (state = initialState, action) => {
    switch (action.type) {
        case "GET_MOVIES": {
            // console.warn("reducer", action.payload)
            return {
                ...state,
                movies: action.payload,
            }
        }
        case "SEARCH": {
            // console.warn("reducer",action.payload)
            return {
                ...state,
                movies: state.movies.filter((obj, index) => {
                    // console.warn(obj.Title,action.payload)
                    return action.payload == obj.imdbID || action.payload == obj.Title
                })
            }
        }
      
        case "SHORTLIST": {
            let copy = []
            let updatedMovies = state.movies.forEach((movie, idx) => {
                movie.imdbID === action.payload ? copy.push({ ...movie, isShortlisted: true }) : copy.push(movie)
            })
            let newArr = []
            for (let i of copy) {
                if (i.isShortlisted === true) {
                    newArr.push(i)
                }
            }
            console.warn(state.imageArr,"Final")
            let oldArr = [...state.imageArr,newArr]
            console.warn('ld arr',oldArr)
            AsyncStorage.setItem("ShortList", JSON.stringify(newArr))
            return {
                ...state,
                movies: copy
            }
        }
        case "SET_IMAGE_DATA": {
            console.warn("reducer", action.payload)

            return {
                ...state,
                imageArr: action.payload
            }
        }
        default: {
            return state;
        }

    }
}

export default getMoviesReducer