const initialState  = {
    status : false
}

const getSearchStatusReducer = (state=initialState,action) => {
    switch(action.type) {
        case "SEARCH_STATUS" : {
            // console.warn("reducer",action.payload)
            return {
                ...state,
                status: action.payload,
            }
        }
         default : {
             return state;
         }   
        
    }
}

export default getSearchStatusReducer