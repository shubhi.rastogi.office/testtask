import React, { Component } from 'react';
import { View, Text } from 'react-native';
import Routes from './src/routes';
// import { SafeAreaProvider, SafeAreaView } from 'react-native-safe-area-context';
import rootReducer from "./src/reducers/index"
 import {createStore} from "redux"
 import {Provider} from "react-redux"


 const store= createStore(rootReducer);
export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <Provider store={store}>
      <View style={{flex:1}}>
           <Routes />
      </View>
     </Provider>
    );
  }
}
